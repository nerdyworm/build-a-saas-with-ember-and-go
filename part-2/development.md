# Development Env


## Rebuilding and restarting
For restarting the server every time there is a code change I like to
use fswatch.  Here is the configuration I am using to get the project
rolling.

[fswatch](https://github.com/codeskyblue/fswatch)

``` js .fswatch.json
{
    "paths": [
        "src"
    ],
    "depth": 10,
    "exclude": [],
    "include": [
        "\\.(go)$"
    ],
    "command": "gb build && bin/ca",
    "env": {
        "POWERD_BY": "github.com/codeskyblue/fswatch"
    },
    "autorestart": false,
    "restart-interval": 0,
    "kill-signal": "KILL"
}
```


## Running all the processes

A little trick that makes running all your development processes a
little easier is called [foreman](https://github.com/ddollar/foreman).

``` ruby Procfile
back: fswatch
dash: cd dashboard && ember server
```

This allows us to run ```foreman start``` and have all our application
processes running in development.

Easy, Breazy.
