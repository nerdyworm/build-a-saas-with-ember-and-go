# Project Overview

This course is going to cover building a complete product with Go and
Ember.js.

There will be the following components
- Ember.js course builder and admin dashboard
- Go backend server
- Go frontend server for delivering courses to students


## Walking Skeleton

The first order of business in any application is to assemble the
walking skeleton.  This should be enough code to verify that each of
the systems are working correctly.  We don't need to write any tests
at this point because we will manually verify the functionality.

## Project Setup

This is going to serve as the root of all our code.

``` sh
$ mkdir course-assembler
```

