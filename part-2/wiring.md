# Wiring

We want to connect our frontend and backend with an api.  In order to
accomplish this our frontend should speak our backend's language.  And
our backend should deal with the real world issues such as cores.

## A Simple Request

Let's build a simple request to list all the course on the server.  Our
app will have to deal with authorization and scoping of courses, but for
now we should be able to wire up the routes on both sides and see the
data flow from one application to the next.

## Dashboard

``` sh
$ ember g route index
$ ember g adapter application
$ ember g serializer application
$ ember g model course
$ ember install go-rest-adapter
```


Add an endpoint to the configuration so that we can wire up our adapter.
```  js app/config/environment.js
if (environment === 'development') {
  // ...
  ENV.endpoint = "http://localhost:3000";
}
```

``` js app/adapters/application.js
import GoRESTAdapter from 'go-rest-adapter/adapter';
import config from 'dashboard/config/environment';

export default GoRESTAdapter.extend({
  host: config.endpoint,
  namespace: 'api/v1',
});
```

``` js app/serializers/application.js
import GoRESTSerializer from 'go-rest-adapter/serializer';

export default GoRESTSerializer.extend();
```

``` js app/models/course.js
import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  name: attr('string'),
});
```


``` js app/routes/index.js
import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.findAll('course');
  }
});
```

Add some content to the index template:
``` hbs app/templates/index.hbs
{{#each model as |course|}}
  <h1>{{course.name}}</h1>
{{/each}}
```

## Backend

``` go
http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
  w.Header().Set("Content-Type", "application/json")
  w.Header().Set("Access-Control-Allow-Origin", "*")
  w.Write([]byte(`{"Courses":[{"ID":"course-id","Name":"Ember and Go"}]}`))
})
```

### Finished

Our frontend should now be able to talk to our backend.  It is fairly
rudementary at the moment, but with some effort we will turn this into a
rock solid platform.
