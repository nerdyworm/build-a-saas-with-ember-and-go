# Go (Backend)

## Prereqs

* Download and install go
* Download and install gb

From the docs of gb _Projects do not live in $GOPATH_.  We are building
a project.  Nothing is going to consume this code as a dependency.

``` sh
$ cd course-assembler
$ mkdir -p src/ca
```

``` go src/ca/main.go
package main

import (
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(`{"hello":"world"}`))
	})

	log.Println("listening port=3000")
	http.ListenAndServe(":3000", nil)
}
```

``` sh
gb build
bin/ca
2016/02/13 10:54:25 listening port=3000
```

Open up http://localhost:3000 and we should see the hello world json
response.
