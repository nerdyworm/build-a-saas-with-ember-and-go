# Ember

* Download and install ember-cli (npm install -g ember-cli@beta)

``` sh
$ cd course-assembler
$ ember new dashboard
$ cd dashbord
$ ember server
```

_At the time of writing you have to use the beta versions of ember-cli
in order to get ember 2.0 as the default_

At this point you should be able to open your browser to http://localhost:4200
and see the welcome page from the ember app.

### Some required libraries

Sometimes we just need to get some stuff out of the way.

``` sh
$ ember install ember-cli-bootstrap-sassy # bootstrap styling
$ ember install ember-cli-sass # sass stylesheets
$ ember install go-rest-adapter # adatper for go's json conventions
```

Let's pretty up the app a bit by addin bootstrap.
``` scss app/styles/app.scss
@import "bootstrap";
@import "bootstrap-theme";
```

Try it: Try adding any of the bootstrap templates to ```app/templates/application.hbs``` to make sure things are styled correctly.

