<div class="welcome row">
  <div class="info-paragraph col-sm-offset-2 col-sm-8">
    <h2 class="text-center">Linear Approach</h2>
    <i class="fa fa-line-chart pull-left text-success"></i>
    <p>
    One of the hardest parts about learning a new stack is the
    figuring out the best ways to move forward.  With this course
    you will never need to have five thousand browser tabs open looking for
    what to do next.  Everything you need to learn about each slice of this
    stack is right infront of you.
    </p>
  </div>
  <div class="info-paragraph col-sm-offset-2 col-sm-8">
    <h2 class="text-center">A complete working project</h2>
    <i class="fa fa-laptop pull-right text-info"></i>
    <p>
    Dive head first into how to ship a real project.  From the
    basic walking skeleton all they way to black box testing you will learn
    every nook and cranny of building production ready applications with
    Ember.js and Go.
    </p>
  </div>
  <div class="info-paragraph col-sm-offset-2 col-sm-8">
    <h2 class="text-center">All the way to production</h2>
    <i class="fa fa-user pull-left text-purple"></i>
    <p>
    We aren't going go leave you hanging with real production
    problems.  This course covers how to delop this app from a single VPS
    server all the way up to CDNs and Auto Scaling with Amazon Web
    Services.
    </p>
  </div>
</div>

<style>
.info-paragraph  {
  padding: 40px 0px;
}

.info-paragraph .fa {
  font-size: 100px;
}

.things {
  text-align: center;
  margin-top: 60px;
  margin-bottom: 100px;
}

.things .fa {
  font-size: 100px;
}

.things h1 {
  font-size: 22px;
  margin-top: 10px;
  marign-bottom: 0px;
  font-weight: normal;
}

.text-purple {
  color: #7b5294
}

.covered-topics {
  padding: 80px 0px;
}

.covered-topics h1 {
  margin-bottom: 30px;
}

.welcome {
  padding: 80px 0px;
}

.welcome h1 {
  margin-bottom: 30px;
}

</style>

<hr>

<div class="covered-topics">
  <h1 class="text-center">
    Checkout the topics that will be covered
  </h1>
  <p class="lead text-center text-muted">
    Learn what it takes to build, deploy, and monitor an app with Ember.js and Go.
  </p>

  <div class="row things">
    <div class="col-sm-3">
      <div class="text-info">
        <i class="fa fa-user"></i>
        <h1>Users</h1>
      </div>
      Sign ups, token based authorization, and much more.
    </div>
    <div class="col-sm-3">
      <div class="text-success">
        <i class="fa fa-money"></i>
        <h1>Payments</h1>
      </div>
      Learn how to implment one time purchases or choose plans recurring
      plans.
    </div>
    <div class="col-sm-3">
      <div class="text-purple">
        <i class="fa fa-cloud"></i>
        <h1>Deploying</h1>
      </div>
      Small VPS to AWS Autoscaling is covered
    </div>
    <div class="col-sm-3">
      <div class="text-danger">
        <i class="fa fa-heartbeat"></i>
        <h1>Testing</h1>
      </div>
      Test the app through and trough
    </div>
  </div>
  <div class="row things">
    <div class="col-sm-3">
      <div class="text-danger">
        <i class="fa fa-clock-o"></i>
        <h1>Automation</h1>
      </div>
      Setup a complete delivery pipeline and continuous integration
    </div>
    <div class="col-sm-3">
      <div class="text-purple">
        <i class="fa fa-users"></i>
        <h1>Development Tricks</h1>
      </div>
      Learn how to build a productive development enviroment
    </div>
    <div class="col-sm-3">
      <div class="text-success">
        <i class="fa fa-cloud-upload"></i>
        <h1>File Uploads</h1>
      </div>
      Implment drag and drop file uploading
    </div>
    <div class="col-sm-3">
      <div class="text-info">
        <i class="fa fa-balance-scale"></i>
        <h1>SOLID</h1>
      </div>
      The solid principles applied to a real code base.
    </div>
  </div>
</div>

<hr>
