# Together As One System

Ember and Go are nothing alike.  One is a huge framework the other is a
small language that frowns upon large frameworks.  However, neither of
them are coupled together.  They are independently deployable systems.
One allows use to rapidly iterate the user interface and the other
allows us to build systems that allow us to sleep at night.

By combining these philosophies and tools we can build amazing things
together.
