# Simplicity

> Why would you have a language that is not theoretically exciting?
> Because it's very useful.
> - Rob Pike

My personal draw to the language Go is that it is exceedingly boring.

Go will not win any productivity wars against in the web development
world, but only in the short run.  Frameworks with Django and Rails will
always be much faster at getting an application out the door.  However a
smart reader will notice that I am not comparing languages, I'm comparing
tools.

Go is a very sharp tool and the philosophy behind Go is to build
on top of the standard library.  Believe it or not, the standard
library is designed so well that it actually makes sense to do so.
Where in other languages huge frameworks must be developed to abstract
away the quirks of their standard library.

At the time of writing there has been no community adoption of a
framework for building web applications in Go.  However there have quite
a few small libraries that add some wonderful functionally to the
standard library.
