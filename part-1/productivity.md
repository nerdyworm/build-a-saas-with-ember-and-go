# Productivity

> Ironically, the existence of frameworks makes keeping up with the
> latest and greatest easier.
> - Tom Dale

Productivity wins.  The team that can ship faster and more reliable
software is going to win in the long rung.

Today, I believe that the largest productivity gains are to be had on
the front end of a web application.  So many stake holders will never
understand or even care about anything other that what they can see and
what they can click on.  We have to embrace this fact and use tools that
allow us to build and change quickly.

Frameworks are fun to use because they embrace the shared problem of
building a application.  As of today, there is only one toolkit that
labels itself as a framework, and that is Ember.js.

The learning curve is steep, but as time goes on and the dust settles
more and more excellent resources will be created to help minimize it.

