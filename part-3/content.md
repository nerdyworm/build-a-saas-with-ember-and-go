Content is king.

``` sh
ember g acceptance-test course-sections
```

``` hbs welcome.hbs
<div class="page-header">
  <h1>Welcome</h1>
</div>

<div class="courses">
  {{#each model as |course|}}
  <div class="course"></div>
  {{else}}
  <div class="welcome-wagon"></div>
  {{/each}}
</div>
```

``` hbs application.hbs
{{outlet}}

<div class="container-fluid footer">
  <div class="row">
    <div class="col-xs-6">
      {{link-to 'About' 'about'}}
    </div>
  </div>
</div>
```
