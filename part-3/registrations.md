# Registrations

``` sh
$ ember g acceptance-test signup
```

At this point the test is failing due to missing the signup path.

Let's generate that.

``` sh
$ ember g route signup
```

The test that I really want to write is that
1 - the correct api request is made
2 - the form displays the errors that the server returns


``` sh
ember install ember-cli-mirage@beta
```
> Note: The beta is required for some features.

ensure that  this is not enabled in development.  Otherwise the library
will not hit our backend server during development.
``` js
// config/environment.js
...
if (environment === 'development') {
  ENV['ember-cli-mirage'] = {
    enabled: false
  }
}
```

``` js
// tests/acceptance/signup-test.js
import { test } from 'qunit';
import moduleForAcceptance from 'dashboard/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | signup');

test('empty signup shows errors', function(assert) {
  assert.expect(2);

  visit('/signup');
  click('button');

  server.post('/api/v1/signups', function(d, r) {
    assert.equal(r.requestBody, '{"Signup":{"Name":null,"Email":null,"Password":null}}');
    return { Errors: { Name: ['required'], Email: ['required'], Password: ['required'] } };
  }, 400);

  andThen(function() {
    assert.equal(currentURL(), '/signup');
  });
});
```

> Pro Tip:  console.log(r.requestBody);  This will give us the input for
> our backend tests.

Let's add a simple handlebars template for the signup form.
``` hbs app/templates/signup.hbs
<form {{action "signup" on="submit"}}>
  <div class="form-group">
    <label class="control-label">Name</label>
    {{input name="name" value=model.name class="form-control"}}
  </div>
  <div class="form-group">
    <label class="control-label">Email</label>
    {{input name="email" value=model.email class="form-control"}}
  </div>
  <div class="form-group">
    <label class="control-label">Password</label>
    {{input name="password" value=model.password type="password" class="form-control"}}
  </div>
  <div class="form-actions">
    <button type="submit" class="btn btn-primary">Sign Up</button>
  </div>
</form>
```

This fails, so let's adjust our sign up route to make it pass

``` js
import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('signup');
  },

  actions: {
    signup() {
      this.currentModel.save().then(
        () => this.transitionTo('check-your-email'),
        Ember.K // prevents invlaid records from raising an exception
      );
    }
  }
});
```

Our first test is here and we are testing that the correct json api
request is made.  This is a very important test because this will
generate our inputs to the backend server.

We are not testing the happy path so let's add another test.

``` js
// tests/acceptance/signup-test.js
// ...

test('complete signup redirects to check-your-email', function(assert) {
  assert.expect(2);

  visit('/signup');
  fillIn('input[name=name]', 'Bob');
  fillIn('input[name=email]', 'bob@example.com');
  fillIn('input[name=password]', 'Password1');
  click('button');

  server.post('/api/v1/signups', function(d, r) {
    assert.equal(r.requestBody, '{"Signup":{"Name":"Bob","Email":"bob@example.com","Password":"Password1"}}');
    return { Signup: { ID: 1, Name: 'Bob', Email: 'bob@example.com', Password: null } };
  }, 201);

  andThen(function() {
    assert.equal(currentURL(), '/check-your-email');
  });
});
```

``` js
// app/router.js
// ...
this.route('check-your-email');
```

``` hbs
<h1>Check your email!</h1>
```

Here we are actually testing that the request contains the correct data
and that if successful we redirect to the check your email route.

## Backend

Our frontend has defined the needs ouf our api.  Basically we have a
POST request with a json body.  We need to test our server to ensure
that we implement that feature well.

``` go
// ca/api/signups_test.go
package api

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestInvalidSignup(t *testing.T) {
	request, _ := http.NewRequest("POST", "/api/v1/signups", strings.NewReader("{}"))
	response := httptest.NewRecorder()

	server := NewServer()
	server.ServeHTTP(response, request)

	if response.Code != 400 {
		t.Fatalf("expected %d got %d", 400, response.Code)
	}
}
```

With a little bit of programming by wishful thinking we have come up
some code that can not compile.  However this is a completely reasonable
way to start writing tests in go.

``` go
// ca/api/server.go
package api

import "net/http"

type Server struct {
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

}

func NewServer() *Server {
	return &Server{}
}
```

With a litte bit of go code we now have a server struct that will allow
our tests to compile, however they do not pass due to the sever returing
the incorrect response for the input.

Now
``` go
// ca/api/server.go
package api

import "net/http"

type Server struct {
  mux *http.ServeMux
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.mux.ServeHTTP(w, r)
}

func NewServer() *Server {
	mux := http.NewServeMux()

	server := &Server{
		mux: mux,
	}

	mux.HandleFunc("/api/v1/signups", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"Errors":{"Email":["required"]}}`))
	})

	return server
}
```

What we did here was add an http.ServerMux to the server which allows
us to route some requests to functions.  Our test now passes.

We used the http.Handler interface so that if we need to move away from
the http.ServerMux we can easily do so with out much hassle. For now the
standard library should work just fine.

Let's update main to use our new server.

``` go
// ca/main.go
package main

import (
	"ca/api"
	"log"
	"net/http"
)

func main() {
	server := api.NewServer()

	log.Println("listening port=3000")
	http.ListenAndServe(":3000", server)
}
```

Our main function really shrunk here.  All we have to do now is create a
new api server and have the http package listen and serve it.

However all is not well in our api, the browser will sent a pre-flight
options request to the server when it feels the need verify security.
Our server fails because the options request needs to return a 200.

We should test this!

``` go
// ca/api/server_test.go
func TestOPTIONS(t *testing.T) {
	request, _ := http.NewRequest("OPTIONS", "/", nil)
	response := httptest.NewRecorder()

	server := NewServer()
	server.ServeHTTP(response, request)

	if response.Code != 200 {
		t.Fatalf("expected %d got %d", 200, response.Code)
	}
}
```
OK now all of our options requests are tested.  We also want to ensure
that no matter what url we hit the options request is respected.

``` go
// ca/api/sever.go
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	header := w.Header()
	header.Set("Access-Control-Allow-Origin", "*")
	if r.Method == "OPTIONS" {
		header.Set("Access-Control-Allow-Headers", "Accept, Content-Type, Authorization")
		header.Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
		header.Set("Access-Control-Max-Age", "600")
		w.WriteHeader(200)
		return
	}

	s.mux.ServeHTTP(w, r)
}
```

We have a centralized area for that, so we can just check the request
before it even makes it to our serve mux.  Sweet.

OK we have the sad path covered, invalid signups will not be an issue.
So now we actually have to validate the real request.

``` go
// ca/api/signups_test.go
func TestValidSignup(t *testing.T) {
	payload := `{"Signup":{"Name":"Bob","Email":"bob@example.com","Password":"Password1"}}`
	request, _ := http.NewRequest("POST", "/api/v1/signups", strings.NewReader(payload))
	response := httptest.NewRecorder()

	server := NewServer()
	server.ServeHTTP(response, request)

	if response.Code != 201 {
		t.Fatalf("expected %d got %d", 201, response.Code)
	}
}
```

Due to the nature of this being the very first piece of functionality
that actually needs to do things like validate the existence of an
email, we are going to take a fairly large leap here.

A lot of folks will write in memory adapters, but for small teams this
becomes an unreasonable burden for most of the basic crud operations.
When the time comes we should create in memory implementations, but that
time is not now, we have to be practical and get this application out
the door to our users.

``` go
type SignupRequest struct {
	Signup struct {
		Name     string
		Email    string
		Password string
	}
}

func (s SignupRequest) Validate() (entities.ValidationErrors, error) {
	errors := entities.NewValidationErrors()

	if s.Signup.Name == "" {
		errors.Add("Name", "Required")
	}

	if s.Signup.Email == "" {
		errors.Add("Email", "Required")
	}

	if s.Signup.Password == "" {
		errors.Add("Password", "Required")
	} else if len(s.Signup.Password) < 8 {
		errors.Add("Password", "Must be at least 8 characters long")
	}

	return errors, nil
}
```

``` go
// ca/entities/validate_errors.go

// ValidationErrors represents entitity validation errors
type ValidationErrors struct {
	Errors map[string][]string
}

// NewValidationErrors returns an empty ValidationErrors
func NewValidationErrors() ValidationErrors {
	return ValidationErrors{
		make(map[string][]string),
	}
}

func (r ValidationErrors) Error() string {
	return fmt.Sprintf("ValidationErrors: %v", r.Errors)
}

// Add adds a new error
func (r *ValidationErrors) Add(name, message string) {
	if r.Errors[name] == nil {
		r.Errors[name] = []string{}
	}

	r.Errors[name] = append(r.Errors[name], message)
}

// Any returns true if contains any errors
func (r ValidationErrors) Any() bool {
	return len(r.Errors) > 0
}
```


``` go
mux.HandleFunc("/api/v1/signups", func(w http.ResponseWriter, r *http.Request) {
  request := SignupRequest{}

  err := json.NewDecoder(r.Body).Decode(&request)
  if err != nil {
    log.Println(err)
    http.Error(w, "Opps", 500)
    return
  }

  errors, err := request.Validate(nil)
  if err != nil {
    log.Println(err)
    http.Error(w, "Opps", 500)
    return
  }

  if errors.Any() {
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusBadRequest)
    json.NewEncoder(w).Encode(errors)
    return
  }

  w.WriteHeader(201)
})
```

We added a new struct named avlidation errors.  This guy should take
care of returning errors in a api friednly format.

The SignupRequest is the struct that we will be working with durring the
quest.

So let's decode the request into a struct named SignupRequest, see if it
validates, if it doesn't then we know what to do, and that is return the
400 and the error messages.  Otherwise everything is cool and we should
return a 201.

Ok we now have a semi working api.  The next thing we have to do is
actually validate if the email address has been taken and save it off
into our database.

> Note:  Instead of pretending like there is never going to be an sql
> database we are just going to roll up our sleeves and deal with the
> reality that memory storage and disk storage do not meet our
> requirements.

Let's grab the postgres driver
``` sh
gb vendor fetch github.com/lib/pq
```

``` go
// ca/db/db.go
package db

import (
	"ca/entities"
	"database/sql"

	_ "github.com/lib/pq"
)

type Database struct {
	*sql.DB
}

func Open(config string) entities.Database {
	db, err := sql.Open("postgres", config)
	if err != nil {
		panic(err)
	}

	return &Database{db}
}
```

We are basically wrapping the sql.DB so that we can add methods to the
database connection itself.  We also want to panic if we can't connect.
The process should just die and something will try to restart it.

``` go
// ca/entities.go
type Database interface {
	Users() Users
	Close() error
}

type Users interface {
	Insert(*User) error
	Exists(email string) (bool, error)
}
```

We need to declare two interfaces for the database.  The first being a
collection of interfaces so that we can segregate data and the second
being the interfaces specific to the user.


``` go
// ca/db/users.go
package db

import "ca/entities"

func (d *Database) Users() entities.Users {
	return &users{d}
}

type users struct {
	*Database
}

func (db *users) Exists(email string) (bool, error) {
	exists := false
	row := db.QueryRow("select exists(select 1 from users where lower(email) = lower($1))", email)
	return exists, row.Scan(&exists)
}

func (db *users) Insert(user *entities.User) error {
	query := "insert into users (name, email, encrypted_password) values($1, lower($2), $3) returning id;"
	return db.QueryRow(query, user.Name, user.Email, user.Password).Scan(&user.ID)
}
```

``` sql
-- ca/db/migrations/users.sql
create table users (
  id bigserial not null primary key,
  name text not null,
  email text not null,
  encrypted_password text not null
);

create unique index users_email_unique on users using btree(email);
```

We implemented the interface using the standard sql library.

Now we can change our validation to include the exists chevk in the
database.

``` go
// ca/api/server.go
func (s SignupRequest) Validate(users entities.Users) (entities.ValidationErrors, error) {
	errors := entities.NewValidationErrors()

	if s.Signup.Name == "" {
		errors.Add("Name", "Required")
	}

	if s.Signup.Email == "" {
		errors.Add("Email", "Required")
	} else {
		exists, err := users.Exists(s.Signup.Email)
		if err != nil {
			return errors, err
		}

		if exists {
			errors.Add("Email", "Already taken")
		}
	}

	if s.Signup.Password == "" {
		errors.Add("Password", "Required")
	} else if len(s.Signup.Password) < 8 {
		errors.Add("Password", "Must be at least 8 characters long")
	}

	return errors, nil
}
```

``` go
// ca/api/server.go

type Server struct {
  mux      *http.ServeMux
	database entities.Database
}

// ...
	mux.HandleFunc("/api/v1/signups", func(w http.ResponseWriter, r *http.Request) {
// ...
		errors, err := request.Validate(server.database.Users())
```
First we need a place to stash the database in the server so that we can
pass it along to the things we need to.  Then we need to update our
validation method call to take in the new parameter.

``` go
// ca/api/server.go
func NewServer(database entities.Database) *Server {
```

We need to update the constructor, this is a dependecny that the server
has no business constructing, so we should pass it in.

``` go
// ca/api/server_test.go
// ...
  db := db.Open("dbname=ca_test sslmode=disable")
  defer db.Close()

  server := NewServer(db)
```

All of our NewServer calls must be updated, any thing you miss the
compiler will yell at you.

``` go
// ca/main.go
package main

import (
	"ca/api"
	"ca/db"
	"log"
	"net/http"
)

func main() {
	database := db.Open("dbname=ca_dev sslmode=disable")
	defer database.Close()

	server := api.NewServer(database)
	log.Println("listening port=3000")
	http.ListenAndServe(":3000", server)
}
```

Our main function should be the one wiring up dependencies


All of our tests should pass.. the first time!

We will have a duplicate email address issue.  I have found the best way
to tackle this is to just embrace the shared cumulative state of the
database and use a random email address for each test.

``` sh
$ gb vendor fetch github.com/tuvistavie/securerandom
```

``` go
// ca/modules/faker/email.go
package faker

import (
	"fmt"

	"github.com/tuvistavie/securerandom"
)

func Email() string {
	uuid, err := securerandom.Uuid()
	if err != nil {
		panic(err)
	}

	return fmt.Sprintf("%s@example.com", uuid)
}
```

When I'm introducing functions that are shared through out the app then
I typically stash them inside a modules folder.  This keeps secondary
stuff out of the main code base and allows us to organize the shared
packages well.


Let's write test for our user database stuff
``` go
// ca/db/users_test.go
package db

import (
	"ca/entities"
	"ca/modules/faker"
	"testing"
)

func TestUsers(t *testing.T) {
	db := Open("dbname=ca_test sslmode=disable")
	defer db.Close()

	email := faker.Email()

	user := &entities.User{
		Name:  "Bob",
		Email: email,
	}

	err := db.Users().Insert(user)
	if err != nil {
		t.Fatal(err)
	}

	exists, err := db.Users().Exists(email)
	if err != nil {
		t.Fatal(err)
	}

	if !exists {
		t.Fatalf("expected %d to exist in the database", email)
	}
}
```

Basically ensuring that what we put in the database actually exists. As
our needs for more database functionality grow so will this test.


Back to our sign up test.  We had it passing before, but did the user
ever make it to the database?
Let's test it.

``` go
func TestValidSignup(t *testing.T) {
	email := faker.Email()
	payload := fmt.Sprintf(`{"Signup":{"Name":"Bob","Email":"%s","Password":"Password1"}}`, email)
	request, _ := http.NewRequest("POST", "/api/v1/signups", strings.NewReader(payload))
	response := httptest.NewRecorder()

	db := db.Open("dbname=ca_test sslmode=disable")
	defer db.Close()

	server := NewServer(db)
	server.ServeHTTP(response, request)

	if response.Code != 201 {
		t.Fatalf("expected %d got %d", 201, response.Code)
	}

	exists, err := db.Users().Exists(email)
	if err != nil {
		t.Fatal(err)
	}

	if !exists {
		t.Fatalf("expected %d to exist in the database", email)
	}
}
```

Survey says no, we never make it to the database.

``` go
// ca/api/server.go

		user := entities.NewUser(
			request.Signup.Name,
			request.Signup.Email,
			request.Signup.Password,
		)

		err = server.database.Users().Insert(user)

```

We need a function like this.  Mostly due to the fact that once we
validate that the user's password is valid, then we should encrypt it as
soon as possible.

``` sh
$ gb vendor fetch golang.org/x/crypto/bcrypt
```

``` go
// ca/entities/user.go
func NewUser(name, email, password string) *User {
	hashed, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	return &User{
		Name:              name,
		Email:             email,
		EncryptedPassword: hashed,
	}
}
```

We panic  because if this ever fails something is terribly wrong we
should just bail.


Now all our tests should be passing!  We are missing one feature,
the JSON response.

Now this is a bit tricky because we really want to clear the password out from
the signup model that was passed to us.

```go
// ca/api/views/signups.go
type Signup struct {
	ID       int64
	Password string
}

type ShowSignup struct {
	Signup Signup
}

func NewShowSignup(user *entities.User) ShowSignup {
	return ShowSignup{
		Signup: Signup{
			ID: user.ID,
		},
	}
}
```

First we declare a view named sign up that will represent the signup model in
our frontend.  We only need to assign the primary key ID and the password.  By
default the password is going to be blank so this is good, however we need to assign
the ID to be the user's id.

``` go
// ca/api/sever.go
// ...
w.WriteHeader(201)
json.NewEncoder(w).Encode(views.NewShowSignup(user))
```

And now we render the view and everything shoudl be good.

One more thing, now we neet to devlier the email to the user.

This somsething that right now... I don't want to implment because it would require
us to actually choose an email provider.  BUt we do want to write the code And
ensure that things are wired up for when we do.

We we don't know the details, we should interface it.

``` go
// ca/modules/mail/sender.go
package mail

import "log"

type Email struct {
	To      string
	From    string
	Subject string
	Message string
}

type Sender interface {
	Send(Email) error
}

type LoggingSender struct{}

func (*LoggingSender) Send(email Email) error {
	log.Printf("to=%s subject=%s message=%s", email.To, email.Subject, email.Message)
	return nil
}

type DummySender struct {
	Last Email
}

func (d *DummySender) Send(email Email) error {
	d.Last = email
	return nil
}
```
For now we will use two types, one that will log and one that we can poke at.

Back to our sign up test
``` go
// ca/api/signup_test.go
func TestValidSignup(t *testing.T) {
	email := faker.Email()
	payload := fmt.Sprintf(`{"Signup":{"Name":"Bob","Email":"%s","Password":"Password1"}}`, email)
	request, _ := http.NewRequest("POST", "/api/v1/signups", strings.NewReader(payload))
	response := httptest.NewRecorder()

	db := db.Open("dbname=ca_test sslmode=disable")
	defer db.Close()

	sender := &mail.DummySender{}

	server := NewServer(db, sender)
	server.ServeHTTP(response, request)

	if response.Code != 201 {
		t.Fatalf("expected %d got %d", 201, response.Code)
	}

	exists, err := db.Users().Exists(email)
	if err != nil {
		t.Fatal(err)
	}

	if !exists {
		t.Fatalf("expected %s to exist in the database", email)
	}

	if sender.Last.To != email {
		t.Fatalf("expected the sender to send to `%s` but got `%s`", email, sender.Last.To)
	}
}
```

We now have a way to inspect out going email from the server.  If we run this it
clearly fails on not sending the correct email.

``` go
// ca/api/server.go
// ...
err = server.sender.Send(views.NewWelcomeEmail(user))
```

In the handler function let's just add the line to the sender and everything should
pass.

``` go
// ca/api/views/user.go
func NewWelcomeEmail(user *entities.User) mail.Email {
	return mail.Email{
		To:      user.Email,
		From:    "noreply@courseassembler.com",
		Subject: "Welcome to Course Assembler",
		Message: "Visit http://localhost:4200/signin",
	}
}
```

In the views package let's add the welcomem email  This should be good
enough to get the ball rolling.  In the future we can render a fancy
html view with the correct url for production.

> Note: We will have to introduce some configuration in order to send email to the
> correct domain.

> Note: We also should actually include a way to confirm the email address
> but that can be left as an exercise to the reader.  It would really only involve
> adding a confirmed flag to the user table and then generate a temp token
> to include in the email. When the user clicks on the link the server would then
> find the user by the token and then update the user record setting the user's
> flag to be confirmed.

## Feature Finished

Well there you have it folks, a complete registraion system written in Go with
a fancy Ember.js frontend.

I'm not going to lie this was a tremendous amount of work, good job.  However we
stayed true to the Go community by not pulling in any needless packages when the
standard library would suffice. We are going to continue to work in this manor
until there is a dire need to clean things up.

## Refactoring our server to suit our needs

 Our http sign up handler has grown rather ugly as we kept introducing new
requirements and more things that return an error.

Let's start with those errors.  It would be really nice to be able to just
return the error to the server and have a centralized error handling location.

``` go
type Handler func(http.ResponseWriter, *http.Request) error

func (s *Server) Handle(path string, fn Handler) {
	s.mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		err := fn(w, r)
		if err == nil {
			return
		}

		switch err.(type) {
		case entities.ValidationErrors:
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(err)
		default:
			log.Printf("error=%s", err)
			http.Error(w, "Error!", 500)
		}
	})
}
```

We basically introduce our applicaiton specific handler type.  This allows us to
return an error from the hanlder so that we have centralized error handling.  So
we can now just return the validation errors from any handler and it will do the
correct thing.

We also wrap this up in a convient way to to create these functions.

``` go
func NewServer(database entities.Database, sender mail.Sender) *Server {
	server := &Server{
		mux:      http.NewServeMux(),
		database: database,
		sender:   sender,
	}

	server.Handle("/api/v1/signups", server.signup)
	return server
}
// ...
func (s *Server) signup(w http.ResponseWriter, r *http.Request) error {
	request := SignupRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		return err
	}

	errors, err := request.Validate(s.database.Users())
	if err != nil {
		return err
	}

	if errors.Any() {
		return errors
	}

	user := entities.NewUser(
		request.Signup.Name,
		request.Signup.Email,
		request.Signup.Password,
	)

	err = s.database.Users().Insert(user)
	if err != nil {
		return err
	}

	err = s.sender.Send(user.Email, "Welcome", "Visit http://localhost:4200/signin")
	if err != nil {
		return err
	}

	w.WriteHeader(201)
	return json.NewEncoder(w).Encode(views.NewShowSignup(user))
}
```

Our NewServer function becomes much more boring now.
