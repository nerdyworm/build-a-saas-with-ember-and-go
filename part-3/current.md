# Current User

We will need a way to look up the current user in our dashboard
application.  One of the best ways to acomplish this is to use a
service.  And the best name for this is going to be the current
service.

``` sh
ember g service current
```

``` js
// tests/unit/services/current-test.js
import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';

moduleFor('service:current', 'Unit | Service | current', {
  needs: ['service:session']
});

test('finds a record from the store and notifies about the change', function(assert) {
  assert.expect(3);

  let user = 'USER';
  let userID = 1;

  let service = this.subject({
    store: {
      findRecord(name, id) {
        assert.equal(id, userID);
        return new Ember.RSVP.resolve(user);
      },
    }
  });

  service.addObserver('user', function() {
    assert.equal(service.get('user'), user);
  });

  service.set('userID', userID);
  assert.equal(service.get('user'), null);
});
```

We do the observer check because our system is all asycn and shit.
Basically the template will automatically update and do it's thing
correctly.

``` js
// app/services/current.js
import Ember from 'ember';

const {
  inject,
  computed,
  isEmpty,
  Service,
  RSVP
} = Ember;
const { service } = inject;
const { oneWay } = computed;

export default Service.extend({
  store: service('store'),
  session: service('session'),
  userID: oneWay('session.data.authenticated.user_id'),

  _user: null,

  user: computed('userID', function() {
    if (this._user === null) {
      this.load();
    }

    return this._user;
  }),

  load() {
    const that = this;
    const userID = this.get('userID');
    if(isEmpty(userID)) {
      return RSVP.resolve(null);
    }

    return this.get('store').findRecord('user', userID).then(function(user) {
      that._user = user;
      that.notifyPropertyChange('user');
      return user;
    });
  },
});
```

Normally we would use a PromiseProxy, but in practice this ends up
behaving weired because we would like to access the current user model
directly, not the proxy object.

Now one the server side we need to implment the api end point.

``` go
// ca/api/users_test.go
package api

import (
	"ca/db"
	"ca/entities"
	"ca/modules/faker"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestUsersShowRequireAuthentication(t *testing.T) {
	request, _ := http.NewRequest("GET", "/api/v1/users/1", nil)
	response := httptest.NewRecorder()

	server := NewServer(nil, nil)
	server.ServeHTTP(response, request)
	if response.Code != http.StatusUnauthorized {
		t.Fatalf("expected a %d got %d", http.StatusUnauthorized, response.Code)
	}
}

```

This fails with a 404, let's implement the endpoint.

``` go
// ca/api/users.go
func (s *Server) user(w http.ResponseWriter, r *http.Request) error {
  user, err := s.CurrentUser(r)
	if err != nil {
		return err
	}

	// currently the only user that will be requested is the current one
  // so let's just show that
	return JSON(w, 200, views.NewShowUser(user))
}
```

``` go
// ca/api/server.go

func NewServer(database entities.Database, sender mail.Sender) *Server {
  // ..
	server.Handle("/api/v1/users/", server.user)
  //..
}
```

``` go
// ca/api/views/user.go
package views

import (
	"ca/entities"
	"ca/modules/mail"
	"crypto/md5"
	"fmt"
	"io"
)

type User struct {
	ID     int64
	Avatar string
	Email  string
	Name   string
}

func NewUser(user *entities.User) User {
	return User{
		ID:     user.ID,
		Avatar: gravatar(user.Email),
		Email:  user.Email,
		Name:   user.Name,
	}
}

type ShowUser struct {
	User User
}

func NewShowUser(user *entities.User) ShowUser {
	return ShowUser{NewUser(user)}
}

func gravatar(email string) string {
	hash := md5.New()
	io.WriteString(hash, email)
	return fmt.Sprintf("//www.gravatar.com/avatar/%x?d=identicon", hash.Sum(nil))
}
```


Our User view is actually a special case.  We do not want to take any
risks by embedding the struct.  We just really don't want to leak the
EncryptedPassword hash or any other important information. We also
want to include other fields like avatar.

``` go
// ca/api/users_test.go
func TestUsersShow(t *testing.T) {
	db := db.Open("dbname=ca_test sslmode=disable")
	defer db.Close()

	email := faker.Email()

	user := entities.NewUser("Bob", email, "Password1")
	db.Users().Insert(user)

	token := entities.NewToken(user.ID)
	db.Tokens().Insert(token)

	request, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/users/%d", user.ID), nil)
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token.Token))

	response := httptest.NewRecorder()

	server := NewServer(db, nil)
	server.ServeHTTP(response, request)
	if response.Code != http.StatusOK {
		t.Fatalf("expected a %d got %d", http.StatusOK, response.Code)
	}
}
```

We should also test that the happy path gets a status ok.

> TODO: How to test the view content?

Back to the dashboard app.

Let's update our sign in test to ensure that the user's name is
displayed.

``` js
// acceptance/signin-test.js
test('it should sign the user in', function(assert) {
  assert.expect(5);

  server.post('/api/v1/tokens', function(d, r) {
    assert.equal(r.requestBody, 'grant_type=password&username=bob%40example.com&password=Password1');
    return { access_token: "access_token", token_type: "Bearer", expires_in: 30, user_id: 1, };
  });

  server.get('/api/v1/users/1', function() {
    assert.ok(true, 'fetch for current user was made');
    return { User: { ID: 1, Name: 'Bob Smith', Avatar: 'http://placehold.it/20x20' } };
  });

  visit('/signin');
  fillIn('input[name=email]', 'bob@example.com');
  fillIn('input[name=password]', 'Password1');
  click('button');

  andThen(function() {
    assert.equal(currentURL(), '/');
    assert.equal(find('.current-user .name').text().trim(), 'Bob Smith');
    assert.equal(find('.current-user .avatar').attr('src'), 'http://placehold.it/20x20');
  });
});
```

``` hbs
{{! app/templates/application.hbs }}
{{#if current.user}}
  <div class="current-user">
    <img src={{current.user.avatar}} class="avatar">
    <span class="name">{{current.user.name}}</span>
  </div>
{{/if}}
```

``` js
// app/controllers/application.js
import Ember from 'ember';

export default Ember.Controller.extend({
  current: Ember.inject.service(),
});
```

YAY the user's name and avatar are now show when the user's is logged
in.

