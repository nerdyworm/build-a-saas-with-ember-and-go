# Create a course


## Dashboard

``` sh
ember g acceptance-test courses
```

``` js
// test/acceptance/courses-test.js
import { test } from 'qunit';
import moduleForAcceptance from 'dashboard/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | courses');

test('visiting / with no courses should show the welcome wagon', function(assert) {
  signIn();
  visit('/');

  andThen(function() {
    assert.equal(currentURL(), '/');
    assert.equal(find('.courses .welcome-wagon').length, 1);
  });
});
```

Basically we want to sign the user into the app and then assert that the welcome
message is show to the user.

``` js
// tests/helpers/sign-in.js
import Ember from 'ember';
import { authenticateSession } from 'dashboard/tests/helpers/ember-simple-auth';

export default Ember.Test.registerHelper('signIn',
  function(app /*, assert, selector, n, context*/) {
    authenticateSession(app, {
      token: 'fake-testing-token',
      user_id: 'test-user',
    });
  }
);
```

```js
// tests/helpers/start-app.js
import './sign-in';
```

``` json
// tests/.jshintrc
{
  "predef": [
    "document",
    "window",
    "location",
    ...
    "signIn",
  ],
  ...
}
```
Our hinter needs to know about the function so that we can use it when ever
we want without it complaining.

``` js
// mirage/config.js
export default function() {
 this.get('/api/v1/users/:id', function(db, request) {
   return {
     "User": {
       "ID": request.params.id,
       "Name": "Bob",
       "Email": "bob@example.com"
     }
   };
 });

 this.get('/api/v1/courses', function() {
   return { Courses: [] };
 });
}
```

We really dont need to test the fetch user any more, so let's just let mirage handle that for us.

``` hbs
<div class="page-header">
  <h1>Welcome</h1>
</div>

<div class="courses">
  {{#each model as |course|}}
  <div class="course"></div>
  {{else}}
  <div class="welcome-wagon"></div>
  {{/each}}
</div>
```

``` js
// test/acceptance/courses-test.js
test('visiting / with one course should not show the welcome wagon', function(assert) {
  signIn();
  visit('/');

  server.get('/api/v1/courses', function() {
    return { Courses: [{ID: 1}] };
  });

  andThen(function() {
    assert.equal(currentURL(), '/');
    assert.equal(find('.courses .welcome-wagon').length, 0);
  });
});
```

## Backend

``` go
// ca/api/courses_test.go
package api

import (
	"ca/db"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCoursesRequiresAuthorization(t *testing.T) {
	server := newTestServer(t)
	defer server.Shutdown()

	request, _ := http.NewRequest("GET", "/api/v1/courses", nil)
	response := httptest.NewRecorder()

	server.ServeHTTP(response, request)

	if response.Code != http.StatusUnauthorized {
		t.Fatalf("expected a %d got %d", http.StatusUnauthorized, response.Code)
	}
}
```

## Database Design

We need to add a few tables to our system to get the database design straight.

``` sql
create table accounts (
  id bigserial not null primary key
);

create table memberships (
  user_id bigint not null references users(id),
  account_id bigint not null references accounts(id)
);

create unique index users_accounts_unique on memberships using btree(user_id, account_id);

create table courses (
  id bigserial not null primary key,
  account_id bigint not null references accounts(id),
  name text not null,
  overview text not null
);

create index courses_accounts on courses using btree(account_id);
```

``` go
// ca/entities/account.go
package entities

type Account struct {
	ID int64
}

type Accounts interface {
	Insert(*Account) error
}
```

``` go
// ca/entities/membership.go
package entities

type Membership struct {
	AccountID int64
	UserID    int64
}

type Memberships interface {
	Insert(*Membership) error
}
```

``` go
// ca/db/accounts_test.go
package db

import (
	"ca/entities"
	"testing"
)

func TestAccounts(t *testing.T) {
	db := newTestDB()
	defer db.Close()

	account := &entities.Account{}
	err := db.Accounts().Insert(account)
	if err != nil {
		t.Fatal(err)
	}

	if account.ID == 0 {
		t.Fatal("expected primary key to be set")
	}
}
```

``` go
// ca/db/accounts.go
package db

import "ca/entities"

func (d *Database) Accounts() entities.Accounts {
	return &accounts{d}
}

type accounts struct {
	*Database
}

func (db *accounts) Insert(account *entities.Account) error {
	query := "insert into accounts default values returning id"
	return db.QueryRow(query).Scan(&account.ID)
}
```

``` go
// ca/db/memberships_test.go
package db

import (
	"ca/entities"
	"testing"
)

func TestMemberships(t *testing.T) {
	db := newTestDB()
	defer db.Close()

	user := &entities.User{}
	err := db.User().Insert(user)
	if err != nil {
		t.Fatal(err)
	}

	account := &entities.Account{}
	err := db.Accounts().Insert(account)
	if err != nil {
		t.Fatal(err)
	}

	membership := &entities.Membership{
		AccountID: account.ID,
		UserID:    user.ID,
	}

	err := db.Memberships().Insert(membership)
	if err != nil {
		t.Fatal(err)
	}
}
```

``` go
// ca/api/signups.go
func (s *Server) signup(w http.ResponseWriter, r *http.Request) error {
  // ...
	err = s.database.Users().Insert(user)
	if err != nil {
		return err
	}

	account := &entities.Account{}
	err = s.database.Accounts().Insert(account)
	if err != nil {
		return err
	}

	membership := &entities.Membership{
		UserID:    user.ID,
		AccountID: account.ID,
	}

	err = s.database.Memberships().Insert(membership)
	if err != nil {
		return err
	}
  // ...
}
```
We need to add our signed up user to an account and then membership.


``` go
// ca/api/courses_test.go
func TestCoursesIndex(t *testing.T) {
	server := newTestServer(t)
	defer server.Shutdown()

	user := entities.NewUser("Bob", faker.Email(), "Password1")
	server.database.Users().Insert(user)

	token := entities.NewToken(user.ID)
	server.database.Tokens().Insert(token)

	request, _ := http.NewRequest("GET", "/api/v1/courses", nil)
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token.Token))
	response := httptest.NewRecorder()

	server.ServeHTTP(response, request)

	if response.Code != http.StatusOK {
		t.Fatalf("expected a %d got %d", http.StatusOK, response.Code)
	}

	view := views.Courses{}
	err := json.NewDecoder(response.Body).Decode(&view)
	if err != nil {
		t.Fatal(err)
	}

	if len(view.Courses) > 0 {
		t.Fatal("expected courses to be empty")
	}
}
```

``` go
package views

import "ca/entities"

type Courses struct {
	Courses []Course
}

type Course struct {
	*entities.Course
}

func NewCourses(courses []*entities.Course) Courses {
	view := Courses{}
	view.Courses = make([]Course, len(courses))
	for i, course := range courses {
		view.Courses[i] = NewCourse(course)
	}
	return view
}

func NewCourse(course *entities.Course) Course {
	return Course{course}
}
```

``` go
// ca/api/courses.go
func (s *Server) courses(w http.ResponseWriter, r *http.Request) error {
	user, err := s.CurrentUser(r)
	if err != nil {
		return err
	}

	courses, err := s.database.Courses().FindForUserID(user.ID)
	if err != nil {
		return s.Report(err)
	}

	return JSON(w, 200, views.NewCourses(courses))
}
```

``` go
// ca/entities/course.go
package entities

type Course struct {
	ID        int64
	AccountID int64
	Name      string
	Overview  string
}

type Courses interface {
	FindForUser(*User) ([]*Course, error)
  Insert(*Course) error
}
```

``` go
// ca/db/courses_test.go
package db

import (
	"ca/entities"
	"testing"
)

func TestCourses(t *testing.T) {
	db := newTestDB()
	defer db.Close()

	user := createUser(db, t)
	account := createAccount(db, t)
	db.Memberships().Insert(&entities.Membership{
		UserID:    user.ID,
		AccountID: account.ID,
	})

	course := &entities.Course{
		Name:      "test course",
		AccountID: account.ID,
	}

	err := db.Courses().Insert(course)
	if err != nil {
		t.Fatal(err)
	}

	if course.ID == 0 {
		t.Fatal("expected primary key to be set")
	}

	courses, err = db.Courses().FindForUser(user)
	if err != nil {
		t.Fatal(err)
	}

	if len(courses) != 1 {
		t.Fatal("expected exactly one course")
	}
}
```

``` go
// ca/db/courses.go
package db

import "ca/entities"

func (d *Database) Courses() entities.Courses {
	return &courses{d}
}

type courses struct {
	*Database
}

func (db *courses) Insert(course *entities.Course) error {
	query := "insert into courses(account_id, name, overview) values($1, $2, $3) returning id"
	return db.QueryRow(query,
		course.AccountID,
		course.Name,
		course.Overview,
	).Scan(&course.ID)
}

func (db *courses) FindForUser(user *entities.User) ([]*entities.Course, error) {
	query := `
		select courses.id, courses.account_id, courses.name, courses.overview from courses
		inner join accounts on courses.account_id = accounts.id
		inner join memberships on accounts.id = memberships.account_id
		where memberships.user_id = $1
	`

	rows, err := db.Query(query, user.ID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	courses := []*entities.Course{}
	for rows.Next() {
		course := &entities.Course{}
		err := rows.Scan(
			&course.ID,
			&course.AccountID,
			&course.Name,
			&course.Overview,
		)

		if err != nil {
			return nil, err
		}

		courses = append(courses, course)
	}

	return courses, nil
}
```

## Creating a course

## Frontend Created
``` js
// tests/acceptane/courses.js
test('create a course', function(assert) {
  assert.expect(2);

  signIn();
  visit('/course-new');

  server.post('/api/v1/courses', function(d, r) {
    assert.equal(r.requestBody, '{"Course":{"Name":"Ember and Go","Overview":"This is the best course in the world"}}');
    return { Course: { ID: 1 }};
  }, 201);

  fillIn('#name', 'Ember and Go');
  fillIn('#overview', 'This is the best course in the world');
  click('button[type=submit]');

  andThen(function() {
    assert.equal(currentURL(), '/courses/1');
  });
});
```

``` js
// app/router.js
this.route('course');
this.route('course-new');
```

``` js
// app/routes/course-new.js
import Authenticated from 'dashboard/routes/authenticated';

export default Authenticated.extend({
  model() {
    return this.store.createRecord('course');
  },

  actions: {
    save() {
      this.currentModel.save().then(
        (c) => this.didSave(c),
        (e) => this.didError(e));
    }
  },

  didSave(course) {
    this.transitionTo('course', course);
  },

  didError() {
    console.log('errored');
  }
});
```


## Showing errors

So we need to display our errors to the user.  Bootstrap has a lovely way
to do this by adding a css class and some text around a text field.  Let's
make a compomnent for this.

``` sh
ember g component form-group
```

``` js
// tests/integration/components/form-group-test.js
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('form-group', 'Integration | Component | form group', {
  integration: true
});

test('has the correct empty state', function(assert) {
  this.set('name', 'email');
  this.set('errors', null);

  this.render(hbs`
    {{#form-group errors=errors name=name}}
      template block text
    {{/form-group}}
  `);

  assert.equal(this.$('.form-group').length, 1);
  assert.equal(this.$('.form-group.has-error').length, 0);
  assert.equal(this.$('.form-group').text().trim(), 'template block text');
});

test('adds the has-error class when the errors contain the name', function(assert) {
  this.set('name', 'email');
  this.set('errors', {
    email: [{ message: 'can not be blank'}, { message: 'another' }]
  });

  this.render(hbs`
    {{#form-group errors=errors name=name}}
      template block text
    {{/form-group}}
  `);

  assert.equal(this.$('.form-group').length, 1);
  assert.equal(this.$('.form-group.has-error').length, 1);
  assert.equal(this.$('.form-group > .help-block').text().trim(), 'can not be blank, another');
});
```

``` js
// app/components/form-group.js
import Ember from 'ember';
const { computed, Component } = Ember;

export default Component.extend({
  classNames: ['form-group'],
  classNameBindings: ['hasError'],
  errors: null,
  name: null,

  hasError: computed('name', 'errors.[]', function() {
    const name = this.get('name');
    return !!this.get('errors.' + name);
  }),

  messages: computed('errors.[]', function() {
    const name = this.get('name');
    const errors = this.get('errors.' + name);
    if (errors) {
      return errors.getEach('message').join(', ');
    }

    return [];
  }),
});
```

``` hbs
// app/templates/course-new.hbs
<div class="page-header">
  <h1>Create a new course</h1>
</div>

<form {{action "save" on="submit"}}>
  {{#form-group errors=model.errors name="name"}}
    <label for="name" class="control-label">Name</label>
    {{input value=model.name class="form-control" elementId="name"}}
    <p class="help-block">A descriptive name for your course</p>
  {{/form-group}}
  {{#form-group errors=model.errors name="overview"}}
    <label for="overview" class="control-label">Overview</label>
    {{textarea value=model.overview class="form-control" elementId="overview"}}
    <p class="help-block">An overivew of what your course is going to teach. <em>(hint: you can edit this later)</em></p>
  {{/form-group}}
  <div class="form-actions">
    <button type="submit" class="btn btn-primary">
      Create Course
    </button>
  </div>
</form>
```

## Backend Created
``` go
// ca/api/courses_test.go
func TestCoursesCreate(t *testing.T) {
	server := newTestServer(t)
	defer server.Shutdown()

	user := entities.NewUser("Bob", faker.Email(), "Password1")
	server.database.Users().Insert(user)

	account := &entities.Account{}
	server.database.Accounts().Insert(account)
	server.database.Memberships().Insert(&entities.Membership{
		AccountID: account.ID,
		UserID:    user.ID,
	})

	token := entities.NewToken(user.ID)
	server.database.Tokens().Insert(token)

	request, _ := http.NewRequest("GET", "/api/v1/courses", strings.NewReader("{}"))
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token.Token))
	response := httptest.NewRecorder()

	server.ServeHTTP(response, request)

	if response.Code != http.StatusBadRequest {
		t.Fatalf("expected a %d got %d", http.StatusBadRequest, response.Code)
	}
}
```

``` go
func (s *Server) courses(w http.ResponseWriter, r *http.Request) error {
	switch r.Method {
	case "POST":
		return s.coursesCreate(w, r)
	default:
		return s.coursesIndex(w, r)
	}
}
```

We need to route our requiest based on the method.  So our courses route becomes
a little swithc statement which calls out to the appiroate function.  Easy peasy.

``` go
type CourseRequest struct {
	Course struct {
		Name     string
		Overview string
	}
}

func (r CourseRequest) Validate() (entities.ValidationErrors, error) {
	errors := entities.NewValidationErrors()

	if r.Course.Name == "" {
		errors.Add("Name", "Can not be blank")
	}

	return errors, nil
}

func (s *Server) coursesCreate(w http.ResponseWriter, r *http.Request) error {
  user, err := s.CurrentUser(r)
  if err != nil {
    return err
  }

	request := CourseRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		return s.Report(err)
	}

	errors, err := request.Validate()
	if err != nil {
		return s.Report(err)
	}

	if errors.Any() {
		return errors
	}

	accounts, err := s.database.Accounts().FindForUser(user)
	if err != nil {
		return s.Report(err)
	}

	// We only allow user's to use one account at this moment
	// Otherwise we would pass it in via the CourseRequest
	account := accounts[0]

	course := &entities.Course{
		AccountID: account.ID,
		Name:      request.Course.Name,
		Overview:  request.Course.Overview,
	}

	err = s.database.Courses().Insert(course)
	if err != nil {
		return s.Report(err)
	}

	return JSON(w, 201, views.NewShowCourse(course))
}
```

``` go
// ca/api/course_test.go
func TestCourseShow(t *testing.T) {
	server := newTestServer(t)
	defer server.Shutdown()

	user := entities.NewUser("Bob", faker.Email(), "Password1")
	server.database.Users().Insert(user)

	account := &entities.Account{}
	server.database.Accounts().Insert(account)
	server.database.Memberships().Insert(&entities.Membership{
		AccountID: account.ID,
		UserID:    user.ID,
	})

	token := entities.NewToken(user.ID)
	server.database.Tokens().Insert(token)

	course := &entities.Course{
		AccountID: account.ID,
		Name:      "Bob's Course",
	}
	server.database.Courses().Insert(course)

	request, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/courses/%d", course.ID), nil)
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token.Token))
	response := httptest.NewRecorder()

	server.ServeHTTP(response, request)

	if response.Code != http.StatusOK {
		t.Fatalf("expected a %d got %d", http.StatusCreated, response.Code)
	}

	view := views.ShowCourse{}
	err := json.NewDecoder(response.Body).Decode(&view)
	if err != nil {
		t.Fatal(err)
	}

	if view.Course.ID != course.ID {
		t.Fatal("exepcted course %d, got  %d", course.ID, view.Course.ID)
	}
}
```

``` go
// ca/api/helpers_test.go
func TestIdFromString(t *testing.T) {
	examples := []struct {
		input    string
		expected int64
	}{
		{"/api/v1/users/1", 1},
		{"/api/v1/users/10", 10},
		{"/api/v1/users/0", 0},
		{"/api/v1/users/1234567890", 1234567890},
		{"/api/v1/users/", -1},
		{"/api/v1/users", -1},
	}

	for i, e := range examples {
		result := idFromString(e.input)
		if result != e.expected {
			t.Errorf("[%d] %s to return %d, got %d", i, e.input, e.expected, result)
		}
	}
}
```
``` go
// ca/api/helpers.go
var idRegex = regexp.MustCompile(`\/(\d+)$`)

func GetID(request *http.Request) int64 {
	return idFromString(request.URL.Path)
}

func idFromString(path string) int64 {
	matches := idRegex.FindAllStringSubmatch(path, 1)

	if len(matches) == 1 {
		i, err := strconv.Atoi(matches[0][1])
		if err != nil {
			return -1
		}

		return int64(i)
	}

	return -1
}
```

``` go
func (s *Server) course(user *entities.User, w http.ResponseWriter, r *http.Request) error {
	id := GetID(r)
	course, err := s.database.Courses().FindForUserByID(user, id)
	if err != nil {
		return s.Report(err)
	}

	return JSON(w, 200, views.NewShowCourse(course))
}
```

``` go
// ca/db/courses.go
func (db *courses) FindForUserByID(user *entities.User, id int64) (*entities.Course, error) {
	courses, err := db.FindForUser(user)
	if err != nil {
		return nil, err
	}

	for i := range courses {
		if courses[i].ID == id {
			return courses[i], nil
		}
	}

	return nil, entities.ErrNotFound
}
```

We now have the basic create and show methods for our courses. 
