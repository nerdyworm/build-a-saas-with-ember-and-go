# Authentication


## Frontend

We should write a test that ensures that the index of our application not displayed
unless the user is signed in.

``` sh
ember g acceptance-test signin
```


``` js test/file.js
import { test } from 'qunit';
import moduleForAcceptance from 'dashboard/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | signin');

test('visiting / requires a valid session', function(assert) {
  visit('/');

  andThen(function() {
    assert.equal(currentURL(), '/signin');
  });
});
```

``` sh
ember install ember-simple-auth
```

``` js app/routes/application.js
import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Ember.Route.extend(ApplicationRouteMixin);
```

``` js app/routes/authenticated.js
import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin);
```


``` js app/routes/index.js
import Authenticated from 'dashboard/routes/authenticated';

export default Authenticated.extend({

});
```

``` js app/routes/unauthenticated.js
import Ember from 'ember';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin);
```

``` js app/routes/signup.js
import Unauthenticated from 'dashboard/routes/unauthenticated'

export default Unauthenticated.extend({
// ...
}});
```

``` js app/router.js
// ...
this.route('signin')
```

```js
test('it should show an error message if it is a bad login', function(assert) {
  assert.expect(3);

  server.post('/api/v1/tokens', function(d, r) {
    assert.equal(r.requestBody, 'grant_type=password&username=bob%40example.com&password=Password1');
    return { message: "invalid email and password" };
  }, 400);

  visit('/signin');
  fillIn('input[name=email]', 'bob@example.com');
  fillIn('input[name=password]', 'Password1');
  click('button');

  andThen(function() {
    assert.equal(currentURL(), '/signin');
    assert.equal(find('.alert').text().trim(), 'invalid email and password');
  });
});
```

``` js app/authorizers/oauth2.js
import OAuth2Bearer from 'ember-simple-auth/authorizers/oauth2-bearer';

export default OAuth2Bearer.extend();
```

``` js app/authenticators/oauth2.js
import OAuth2PasswordGrant from 'ember-simple-auth/authenticators/oauth2-password-grant';
import config from 'dashboard/config/environment';

export default OAuth2PasswordGrant.extend({
  serverTokenEndpoint: config.endpoint + '/api/v1/tokens',
  clientId: 'dashboard',
});
```


``` js app/adapters/application.js
import GoRESTAdapter from 'go-rest-adapter/adapter';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import config from 'dashboard/config/environment';

export default GoRESTAdapter.extend(DataAdapterMixin, {
  authorizer: 'authorizer:oauth2',
  host: config.endpoint,
  namespace: 'api/v1',
});
```

``` hbs templates/signin2.hbs
{{#if message}}
  <div class="alert alert-danger">
    {{message}}
  </div>
{{/if}}

<form {{action "signin" on="submit"}}>
  <div class="form-group">
    <label class="control-label">Email</label>
    {{input name="email" value=email class="form-control"}}
  </div>
  <div class="form-group">
    <label class="control-label">Password</label>
    {{input name="password" value=password type="password" class="form-control"}}
  </div>
  <div class="form-actions">
    <button type="submit" class="btn btn-primary">Sign In</button>
  </div>
</form>
```

``` js app/controllers/signin.js
import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service(),
  email: null,
  password: null,
  message: null,


  actions: {
    signin() {
      this.set('message', null);
      this.get('session').authenticate('authenticator:oauth2', this.get('email'), this.get('password')).
        then(() => this.reset(), (error) => this.showError(error));
    }
  },

  showError(error) {
    this.setProperties(error);
  },

  reset() {
    this.setProperties({
      email: null,
      password: null,
      message: null,
    });
  }
});
```


``` js
test('it should sign the user in', function(assert) {
  assert.expect(2);

  server.post('/api/v1/tokens', function(d, r) {
    assert.equal(r.requestBody, 'grant_type=password&username=bob%40example.com&password=Password1');
    return {
      access_token: "access_token",
      token_type: "Bearer",
      expires_in: 30,
      user_id: 1,
    };
  }, 200);

  visit('/signin');
  fillIn('input[name=email]', 'bob@example.com');
  fillIn('input[name=password]', 'Password1');
  click('button');

  andThen(function() {
    assert.equal(currentURL(), '/');
  });
});
```

## Backend

> Note: It is beyond the scope of this course to fully implement a complete oauth2
> system.  However as long as you are serving over https then our basic token based
> authentication system will work just fine.  Caveat being that the token does not
> leak.

Our API needs be stateless, therefore we need to include all the information
necessary to identify a user making a request.  To accomplish this we will implement
a basic token system.

The token flow is straight forward
1. User provides their credentials, in our case an email address and password
2. We issue a insert a randomly generated token into our database that maps to the user it belongs to
3. Each api request will include that token (already taken care of by simple auth)

### /api/v1/tokens

Our first test is always going to be the super unhappy path.  In this case some
random post request to our tokens end point.  Our server should expect this to
happen from time to time and we should just return an invalid request.

``` go ca/api/tokens_test.go
func TestInvalidTokenGrant(t *testing.T) {
	request, _ := http.NewRequest("POST", "/api/v1/tokens", strings.NewReader(""))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	response := httptest.NewRecorder()

	db := db.Open("dbname=ca_test sslmode=disable")
	defer db.Close()

	server := NewServer(db, nil)
	server.ServeHTTP(response, request)

	if response.Code != 400 {
		t.Fatalf("expected a 400 got %d", response.Code)
	}
}
```

``` go ca/api/tokens.go
func (s *Server) tokens(w http.ResponseWriter, r *http.Request) error {
	return JSON(w, 400, views.InvalidLogin)
}
```

Add the the error message view
``` go ca/api/views/tokens.go
var (
	InvalidLogin = map[string]interface{}{
		"message": "Please check your username and password",
	}
)
```

This should be just enough code to make our degenerate test case pass.


Now that that is cool let's jump into the happy path, a valid token grant.

Our test setup requires us to create a new user and stuff.
``` go ca/api/tokens_test.go
func TestValidTokenGrant(t *testing.T) {
	email := faker.Email()
	payload := fmt.Sprintf("grant_type=password&username=%s&password=Password1", email)

	request, _ := http.NewRequest("POST", "/api/v1/tokens", strings.NewReader(payload))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	response := httptest.NewRecorder()

	db := db.Open("dbname=ca_test sslmode=disable")
	defer db.Close()

	user := entities.NewUser("Bob", email, "Password1")
	db.Users().Insert(user)

	server := NewServer(db, nil)
	server.ServeHTTP(response, request)

	if response.Code != 201 {
		t.Fatalf("expected a 201 got %d", response.Code)
	}

	token := views.Token{}
	json.NewDecoder(response.Body).Decode(&token)
	if token.UserID != user.ID {
		t.Fatal("expected the user id to be included got %d", token.UserID)
	}
}
```

``` go ca/api/tokens.go
func (s *Server) tokens(w http.ResponseWriter, r *http.Request) error {
	err := r.ParseForm()
	if err != nil {
		return err
	}

	if r.FormValue("grant_type") != "password" {
		return JSON(w, 400, views.InvalidLogin)
	}

	user, err := s.database.Users().FindByEmail(r.FormValue("username"))
	if err == entities.ErrNotFound {
		return JSON(w, 400, views.InvalidLogin)
	}

	if err != nil {
		return err
	}

	if !user.CheckPassword(r.FormValue("password")) {
		return JSON(w, 400, views.InvalidLogin)
	}

	access := entities.NewToken(user.ID)
	err = s.database.Tokens().Insert(access)
	if err != nil {
		return err
	}

	return JSON(w, http.StatusCreated, views.NewToken(access))
}
```

``` go ca/api/server.go

func NewServer(database entities.Database, sender mail.Sender) *Server {
  // ..
	server.Handle("/api/v1/tokens", server.tokens)
  //..
}
```

We are going to make a little jump here and add a finder method that we will need
very soon.  They are quite simular as the only thing that changes is the where
clause.
``` go ca/entities/user.go
type Users interface {
  // ...
  FindByEmail(string) (*User, error)
  FindByID(int64) (*User, error)
}
```

``` go ca/db/users_test.go
func TestUsers(t *testing.T) {
  // ...
  found, err := db.Users().FindByEmail(email)
  if err != nil {
    t.Fatal(err)
  }

  if found.ID != user.ID {
    t.Fatal("found wrong user")
  }

  found, err = db.Users().FindByID(user.ID)
  if err != nil {
    t.Fatal(err)
  }

  if found.ID != user.ID {
    t.Fatal("found wrong user")
  }
}
```

Add to our users test, make sure we get it covered.

Continue adding to our users database.

``` go ca/db/users.go
func (db *users) FindByEmail(email string) (*entities.User, error) {
	query := "select id, name, email, encrypted_password from users where lower(email) = lower($1)"
	return db.queryUser(query, email)
}

func (db *users) FindByID(id int64) (*entities.User, error) {
	query := "select id, name, email, encrypted_password from users where id = $1"
	return db.queryUser(query, id)
}

func (db *users) queryUser(query string, args ...interface{}) (*entities.User, error) {
	user := &entities.User{}

	err := db.QueryRow(query, args...).Scan(
		&user.ID,
		&user.Name,
		&user.Email,
		&user.EncryptedPassword,
	)

	if err == sql.ErrNoRows {
		return nil, entities.ErrNotFound
	}

	return user, nil
}
```

Now that we have found the user we have to check it's password.
``` go ca/entities/user.go
func (u *User) CheckPassword(password string) bool {
	if len(password) < 8 {
		return false
	}

	return bcrypt.CompareHashAndPassword(u.EncryptedPassword, []byte(password)) == nil
}
```

We first want to ensure that the incoming password is at least 8 chars.  If that
checks out we can then use bcrypt to compare the EncryptedPassword with the
incoming password.


``` go ca/entities/token.go
package entities

import (
	"encoding/base64"
	"time"

	"github.com/tuvistavie/securerandom"
)

type Token struct {
	Token     string
	UserID    int64
	CreatedAt time.Time
}

type Tokens interface {
	DeleteByToken(string) error
	DeleteByUserID(int64) error
	FindByToken(string) (*Token, error)
	Insert(*Token) error
}

func NewToken(id int64) *Token {
	return &AccessToken{
		Token:  newRandomToken(),
		UserID: id,
	}
}

func newRandomToken() string {
	uuid, err := securerandom.Uuid()
	if err != nil {
		panic(err)
	}

	return base64.URLEncoding.EncodeToString([]byte(uuid))
}
```
Our token entitiy is a simple random number that maps to the user we want to
map it to.

``` go ca/db/tokens_test.go
package db

import (
	"ca/entities"
	"ca/modules/faker"
	"testing"
)

func TestTokens(t *testing.T) {
	db := Open("dbname=ca_test sslmode=disable")
	defer db.Close()

	email := faker.Email()
	user := &entities.User{Name: "Bob", Email: email}

	err := db.Users().Insert(user)
	if err != nil {
		t.Fatal(err)
	}

	token1 := entities.NewToken(user.ID)
	token2 := entities.NewToken(user.ID)
	err = db.Tokens().Insert(token1)
	if err != nil {
		t.Fatal(err)
	}

	err = db.Tokens().Insert(token2)
	if err != nil {
		t.Fatal(err)
	}

	_, err = db.Tokens().FindByToken(token1.Token)
	if err != nil {
		t.Fatal(err)
	}

	err = db.Tokens().DeleteByToken(token1.Token)
	if err != nil {
		t.Fatal(err)
	}

	_, err = db.Tokens().FindByToken(token1.Token)
	if err != entities.ErrNotFound {
		t.Fatalf("Not found tokens should have returned ErrNotFound got %s", err)
	}

	err = db.Tokens().DeleteByUserID(user.ID)
	_, err = db.Tokens().FindByToken(token2.Token)
	if err != entities.ErrNotFound {
		t.Fatalf("Not found tokens should have returned ErrNotFound got %s", err)
	}
}
```

We need to fully exercise our interface, so let's write a test that will do that.

``` go ca/db/tokens.go
package db

import (
	"ca/entities"
	"database/sql"
)

func (d *Database) Tokens() entities.Tokens {
	return &tokens{d}
}

type tokens struct {
	*Database
}

func (db *tokens) FindByToken(token string) (*entities.Token, error) {
	access := &entities.Token{}

	query := "select token, user_id, created_at from access_tokens where token = $1 limit 1"
	err := db.QueryRow(query, token).Scan(
		&access.Token,
		&access.UserID,
		&access.CreatedAt,
	)

	if err == sql.ErrNoRows {
		return nil, entities.ErrNotFound
	}

	return access, err
}

func (db *tokens) DeleteByToken(token string) error {
	query := "delete from access_tokens where token = $1"
	_, err := db.Exec(query, token)
	return err
}

func (db *tokens) DeleteByUserID(id int64) error {
	query := "delete from access_tokens where user_id = $1"
	_, err := db.Exec(query, id)
	return err
}

func (db *tokens) Insert(access *entities.Token) error {
	query := "insert into access_tokens (token, user_id) values($1, $2)"
	_, err := db.Exec(query, access.Token, access.UserID)
	return err
}
```

The implementation that will make the test pass.

``` go ca/api/views/tokens.go
type Token struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
	UserID      int64  `json:"user_id"`
}

func NewToken(access *entities.Token) Token {
	return Token{
		AccessToken: access.Token,
		ExpiresIn:   entities.DefaultExpiresIn,
		TokenType:   "Bearer",
		UserID:      access.UserID,
	}
}
```
Last but not least is the view.

``` go ca/api/server.go
func (s *Server) CurrentUser(r *http.Request) (*entities.User, error) {
	var token string

	parts := strings.Split(r.Header.Get("Authorization"), " ")
	if len(parts) != 2 {
		return nil, entities.ErrInvalidToken
	}

	token = parts[1]
	if token == "" {
		return nil, entities.ErrInvalidToken
	}

	access, err := s.database.Tokens().FindByToken(token)
	if err != nil {
		return nil, err
	}

	if access.IsExpired() {
		return entities.ErrTokenExpired
	}

	user, err := s.database.Users().FindByID(access.UserID)
	if err != nil {
		return nil, err
	}

	return user, nil
}
```

``` go ca/entities/tokens_test.go
package entities

import (
	"testing"
	"time"
)

func TestTokenExpires(t *testing.T) {
	token := Token{CreatedAt: time.Now()}
	if token.IsExpired() {
		t.Fatal("new tokens should not be expired")
	}

	token.CreatedAt = time.Now().Add(-1 * time.Year)
	if !token.IsExpired() {
		t.Fatal("old tokens should be expired")
	}
}
```

``` go ca/entities/tokens.go
var (
	DefaultExpiresIn = 60 * 60 * 24 * 30
)

func (t *Token) IsExpired() bool {
	deadline := time.Now().Unix() - int64(DefaultExpiresIn)
	return t.CreatedAt.Unix() < deadline
}
```

Test to make sure our tokens expire at some point.  30 days is reasonale to make
people login again.

We now have a way of athetnicating the user.

### Signout

Simple auth will actually send a sign out request to the server, let's impoement that.

``` go
func TestValidTokenGrant(t *testing.T) {
  // ...

	payload = fmt.Sprintf("token=%s", token.AccessToken)
	request, _ = http.NewRequest("POST", "/api/v1/tokens/revoke", strings.NewReader(payload))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	response = httptest.NewRecorder()
	server.ServeHTTP(response, request)

	if response.Code != http.StatusUnauthorized {
		t.Fatalf("expected a %d got %d", http.StatusUnauthorized, response.Code)
	}

	request, _ = http.NewRequest("POST", "/api/v1/tokens/revoke", strings.NewReader(payload))
	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token.AccessToken))

	response = httptest.NewRecorder()
	server.ServeHTTP(response, request)

	if response.Code != http.StatusNoContent {
		t.Fatalf("expected a %d got %d", http.StatusNoContent, response.Code)
	}

	_, err := server.database.Tokens().FindByToken(token.AccessToken)
	if err != entities.ErrNotFound {
		t.Fatal("Expected the token to no longer exist")
	}
}
```

At the end of our vlaidd token grant test, let's ensure that we can get authorized
access to the server and then delete our token.

We also throw in the we forgot to put our token test in so we test
authorization.

``` go
func (s *Server) tokensRevoke(w http.ResponseWriter, r *http.Request) error {
  _, err := s.CurrentUser(r)
	if err != nil {
		return err
	}

	err := s.database.Tokens().DeleteByToken(r.FormValue("token"))
	if err != nil {
		return err
	}

	w.WriteHeader(http.StatusNoContent)
	return nil
}
```

Let's add a sign out button and intergration test it.  Sign out isn't
that imporant so we are going to shove it under the profile tag.

``` hbs app/templates/profile.hbs
<button class="btn btn-default" {{action "signout"}}>
  Sign out of this device
</button>
```

``` js app/routes/profile.js
import Ember from 'ember';
import Authenticated from 'dashboard/routes/authenticated';

const { inject } = Ember;
const { service } = inject;

export default Authenticated.extend({
  session: service(),
  actions: {
    signout() {
      this.get('session').invalidate();
    },
  }
});
```

``` js app/router.js
this.route('profile');
```
