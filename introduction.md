# Introduction

Stability, productivity, and beauty are the key elements of success of
any modern web application.  What ever system we build the stakeholders
and users have to trust that their work is always going to be available
to them. A productive and happy system for programmers is now mandated.
We simply can not produce beautiful applications with out being able to
throw all of our ideas against the wall quickly.

The tools we choose actually matter.  When we choose a tool we are
choosing a community to work with, to draw talent from, and to embrace
all the idiosyncrasies that come along with it.  You can build anything
with any language, framework, or tool but at the end of the day you have
to work with other people.  And working with other like minded people is
the key to success in any project.

## The Course that I wanted to exist

I struggled for a long time to learn the best practices of each of these
tools.  I simply wanted a book like this to exist, however no one was
going to to do but I.   So I rolled up my sleves, failed several times
to write it and now.. I'm actually writing it.  I guess the saying is
true, you have to write the books that you want to read.

